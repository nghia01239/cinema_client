<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="top-header span_top">
		<div class="logo">
			<a href="index.html"><img src="${pageContext.request.contextPath }/resources/images/logo.png" alt=""></a>
			<p>Movie Theater</p>
		</div>
		
		<div class="clearfix"></div>
	</div>
<br>
<br>
<img
	src="${ pageContext.request.contextPath }/resources/uploads/images/${ user.avatar }"
	width="150" height="150" alt="Avatar"
	style="border-radius: 80px; display: block; margin-right: auto; margin-left: auto;" />
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-3 col-md-3 col-lg-3"></div>
		<div class="col-6 col-md-6 col-lg-6">
			<div style="margin-bottom: 100px; display: block;">

				<table class="table table-bordered table-striped">
					<tr>
						<th>Username:</th>
						<td>${user.username }</td>
					</tr>
					<tr>
						<th>Email:</th>
						<td>${user.email }</td>
					</tr>
					<tr>
						<th>Fullname:</th>
						<td>${user.fullname }</td>
					</tr>
					<tr>
						<th>Birthday:</th>
						<td>${user.birthday }</td>
					</tr>
					<tr>
						<th>City:</th>
						<td>${user.city }</td>
					</tr>
					<tr>
						<th>Phone:</th>
						<td>${user.phone }</td>
					</tr>
					<tr>
						<th>Gender:</th>
						<td>${user.gender }</td>
					</tr>

				</table>


				<div class="form-row" style="margin-left: 15px;">
					<a href="${pageContext.request.contextPath }/account/booking"
						class="btn btn-success"> Your Booking </a> <a
						href="${ pageContext.request.contextPath }/account/change"
						class="btn btn-primary">Change Infomation</a> <a
						href="${ pageContext.request.contextPath }/account/logout"
						class="btn btn-danger">Logout</a>
				</div>

			</div>
		</div>
	</div>
</div>